package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
	    public void printMenu()
	    {
	    	System.out.println("1.  R1 Cargar la información");
	    	System.out.println("2.  1A Obtener las N letras más frecuentes por las que comienza el nombre de una zona (No diferenciar las mayúsculas de las minúsculas)");
	    	System.out.println("3.  2A Buscar los nodos que delimitan las zonas por Localización Geográfica (latitud, longitud).");
	    	System.out.println("4.  3A Buscar los tiempos promedio de viaje que están en un rango y que son del primer trimestre del 2018.");
	    	System.out.println("5.  1B Buscar las N zonas que están más al norte. ");
	    	System.out.println("6.  2B Buscar nodos de la malla vial por Localización Geográfica (latitud, longitud). ");
	    	System.out.println("7.  3B Buscar los tiempos de espera que tienen una desviación estándar en un rango dado y que son del primer trimestre del 2018.");
	    	System.out.println("8.  1C Retornar todos los tiempos de viaje promedio que salen de una zona dada y a una hora dada.");
	    	System.out.println("9.  2C Retornar todos los tiempos de viaje que llegan de una zona dada y en un rango de horas.");
	    	System.out.println("10. 3C Obtener las N zonas priorizadas por la mayor cantidad de nodos que definen su frontera");
	    	System.out.println("11. 4C Gráfica ASCII - Porcentaje de datos faltantes para el primer semestre 2018");
	    	System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	    }

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			System.out.println(modelo);	
			
		}
		
		
		
		
		
		
}
