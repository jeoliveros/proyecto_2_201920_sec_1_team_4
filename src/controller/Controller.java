package controller;

import java.io.IOException;
import java.util.Scanner;

import com.google.gson.JsonArray;

import model.data_structures.ArbolBalanceadoRojoNegro;
import model.data_structures.ListaEnlazada;
import model.data_structures.MaxHeapCP;
import model.data_structures.TablaHashSC;
import model.logic.MVCModelo;
import model.logic.ZonasUber;
import model.logic.ZonasUber2;
import model.logic.mallaVial;
import model.logic.viajeUberHora;
import model.logic.viajeUberMes;
import view.MVCView;


public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		
		System.out.println("Constructor Controller");
		
		view = new MVCView();
		
		System.out.println("Construy� el MVCView");
		
		modelo = new MVCModelo();
		
		System.out.println("Construy� el MVCModelo");
	}

	public void run() 
	{
		System.out.println("Entra al run");

		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";



		while( !fin ){
			
			System.out.println("Antes de imprimir el menu");
			
			view.printMenu();

			System.out.println("Despu�s de imprimir el menu");
			
			int option = lector.nextInt();
			switch(option){
			case 1:


				view.printMessage("Se empezaron a cargar los datos");

			    //String numeroDeViajes = modelo.cargarDatosViajes();
				
				//int numeroDeMallas = modelo.cargarMallaVial();
				
				//int numerodezonasJson = (int) modelo.leerDatosJson();
				
				
				//view.printMessage("1. "+ numeroDeViajes);

				//view.printMessage("El número de nodos (esquinas) de la malla vial del archivo TXT es: "+ numeroDeMallas);

				//view.printMessage(" El número de zonas que se cargaron del archivo JSON es: " + numerodezonasJson);



				break;

			case 2:
				view.printMessage("Ingrese la cantidad de letras que quiere imprimir que son las m�s po�pulares");

				int n = lector.nextInt();
				

				String [] resultado= modelo.requerimiento1A(n);
				

				
				break;

			case 3:
				view.printMessage("Ingrese  el rango de zonas latitud ");
				int platitud = Integer.parseInt(lector.next());
				
				view.printMessage("Ingrese  el rango de zonas longitud ");
				int plongitud = Integer.parseInt(lector.next());
				modelo.requerimientoA2(platitud, plongitud);
				


				break;

			case 4:
				view.printMessage("Ingresar el limite bajo  ");
				int  tiempoBajo = Integer.parseInt(lector.next());
				view.printMessage("Ingresar el limite alto  ");
				int tiempoAlto = Integer.parseInt(lector.next());
				view.printMessage("Ingresar la cantidad de viajes ");
				int n3A = Integer.parseInt(lector.next());
				modelo.requerimiento3A( tiempoBajo, tiempoAlto, n3A);


				break;

			case 5:

				//R1B
				view.printMessage("Ingrese el numero de zonas que desea encontrar");

				int numeroZonas = Integer.parseInt(lector.next());

				//mostar ordenadas de las que estan mas al norte 


				ZonasUber[] zonasMasAlNorte = modelo.R1B_BuscarLasNZonasMasAlNorte(numeroZonas);
				
				
				ZonasUber[] ZonasOrdenadas = new ZonasUber[zonasMasAlNorte.length];
				
				//ordena las zonas
				for (int i = 0; i < zonasMasAlNorte.length; i++) 
				{
					double a = modelo.buscaMayorLatitudEnArregloDePuntosDeUnaZona(zonasMasAlNorte[i].getArreglopuntos());

					 ZonasOrdenadas = modelo.ordenarDeMayorAMenorMerge( a , zonasMasAlNorte) ;
					
				}
				
	
				
				//imprimir el nombre y la (latitud, longitud) de su punto más al norte
				for (int j = 0; j < ZonasOrdenadas.length; j++) 
				{
					ZonasUber actual = ZonasOrdenadas[j];

					String nombreZona = actual.getScanombre();
					
					
					JsonArray arregloConPuntos = actual.getArreglopuntos();
					
					double  latitud = modelo.buscaMayorLatitudEnArregloDePuntosDeUnaZona(arregloConPuntos) ;
					double  longitud = 0;
					
					for (int jk = 0; jk < arregloConPuntos.size(); jk++) 
					{
						String longiActual = arregloConPuntos.getAsJsonArray().get(0).getAsString();
						String latitudActual = arregloConPuntos.getAsJsonArray().get(1).getAsString();

						double longitudactual = Double.parseDouble(longiActual);
						double latitudActuall = Double.parseDouble(latitudActual);

						if (latitud == latitudActuall )
						{
							longitud = longitudactual;
						}
						
					}
					

					view.printMessage("El nombre la zona "+j+" Es:"+ nombreZona+" y la latiutd y longuitud de su punto mas al norte es"+latitud+","+ longitud  );
				}

				break;

			case 6:

				//R2B
				view.printMessage("Ingrese la longitud a buscar ");
				double LongitudDada = Double.parseDouble(lector.next());

				view.printMessage("Ingrese la latitud a buscar");
				double LatitudDada = Double.parseDouble(lector.next());
				
				
				int NumeroDeNodosRespuesta = 20;

				
				mallaVial[]  listaConMallasSimilares = modelo.R2B_BuscarNodosMallaVial(LongitudDada, LatitudDada, NumeroDeNodosRespuesta);
				//Se debe mostrar el número de nodos retornados y de cada nodo su id, latitud y longitud.

	
				
				view.printMessage("El numero de nodos retornados es: "+listaConMallasSimilares.length );

				for (int i = 0; i < listaConMallasSimilares.length; i++) {

					mallaVial actual = listaConMallasSimilares[i];
					
					int idNodoa = actual.darIdNodo() ;
					double longituda = actual.darlongitud();
					double latituda = actual.darlatitud();

					view.printMessage("El id del nodo es: "+idNodoa +" Su longitud es:"+ longituda+ " Su latitud es: "+ latituda);
				}


				break;

			case 7:
				//R3B
				
				view.printMessage("Ingrese el limite bajo del rango de desviaciones estandar que desea buscar ");
				double limiteBajoDesviacion = Double.parseDouble(lector.next());

				view.printMessage("Ingrese el limite alto del rango de desviaciones estandar que desea buscar");
				double limiteAltoDesviacion = Double.parseDouble(lector.next());

				view.printMessage("Ingrese el numero de datos que desea encontrar");
				int numeroDatos = Integer.parseInt(lector.next());

				
				
				//ordenados por zona de origen y zona de destino
				for (int i = 0; i < numeroDatos; i++) 
				{
				
					viajeUberMes prueba = modelo.R3B_BuscarTiemposDeEsperaConRango(limiteBajoDesviacion, limiteAltoDesviacion, numeroDatos)[i];
					
					
					int zonaOrigen7 = prueba.darsourceId();
					int zonaDestino7 = prueba.dardestinID();
					int mes = prueba.darMes();
					double desviacionestandarViaje7 = prueba.darstandardDeviationTravelTime();

					view.printMessage(i+": La zona de origen es: "+zonaOrigen7 +" La zona destino es: "+ zonaDestino7 +" El mes es: "+ mes+" Su desviacion estandar es: "+ desviacionestandarViaje7);
				}

				break;


			case 8:
				
				view.printMessage("Ingrese la zona de salida");
				int zonaDada8 = Integer.parseInt(lector.next());
				
				view.printMessage("Ingrese la hora");
				int HoraDada8 = Integer.parseInt(lector.next());
				
				ListaEnlazada<viajeUberHora> rta = modelo.requerimientos1C(zonaDada8, HoraDada8);
				
				for(int i=0; i<rta.size(); i++)
				{
					viajeUberHora actual = rta.buscar(i);
					
					if(actual==null)
						continue;
					
					String linea = "";
					linea += (i+1)+".";
					linea += "Zona origen: " + actual.darsourceId();
					linea += " - Zona destino: " + actual.dardestinID();
					linea += " - Hora: " + actual.darHod();
					linea += " - Promedio: " + actual.darmeanTravelTime();
					
					System.out.println(linea);
				}

				break;



			case 9:

				view.printMessage("Ingrese el id de la zona de llegada");
				int idZonallegada9 = Integer.parseInt(lector.next());
				
				view.printMessage("Ingrese la hora inferior del rango");
				int HoraDadaMayor9 = Integer.parseInt(lector.next());
				
				view.printMessage("Ingrese la hora superior del rango");
				int HoraDadaMenor9 = Integer.parseInt(lector.next());
				
				ListaEnlazada<viajeUberHora> Req_2C = modelo.requerimientoParteC2(idZonallegada9, HoraDadaMenor9, HoraDadaMayor9);
				
				for(int i=0; i<Req_2C.size(); i++)
				{
					viajeUberHora actual = Req_2C.buscar(i);
					
					if(actual==null)
						continue;
					
					String linea = "";
					linea += (i+1)+".";
					linea += "Zona origen: " + actual.darsourceId();
					linea += " - Zona destino: " + actual.dardestinID();
					linea += " - Hora: " + actual.darHod();
					linea += " - Promedio: " + actual.darmeanTravelTime();
					
					System.out.println(linea);
				}
				
				break;



			case 10:
				//R3C
				
				view.printMessage("Ingrese el numero de datos que desea encontrar");
				int numeroDatos10 = Integer.parseInt(lector.next());
				
				
				for (int i = 0; i < numeroDatos10; i++) {
					
					ZonasUber2 actual = modelo.R3C_ObetenerLasNZonasPriorizadas(numeroDatos10)[i];
					
					
					String nombreZona = actual.getScanombre() ;
					
					
					int numeroNodosFronteras = actual.getArreglopuntos().size();
					
					view.printMessage("El nombre de la zona es: "+nombreZona+". El número de nodos que definen su frontera es: "+numeroNodosFronteras);
				}
				
				
				

				break;



			case 11:
				//R4C

				view.printMessage("Porcentaje de datos faltantes por zona"); 
				
				
				for (int i = 0; i < 1160; i++) 
				{
					
				    int	NumeroViajesNoExistentes= modelo.R4C_RetornaElNumeroDeDatosFaltantes(i);
					
					view.printMessage(i+ " | "+ modelo.graficaAsterisco(NumeroViajesNoExistentes));
					
				}
				
				break;



			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}
		
		lector.close();

	}	
}
