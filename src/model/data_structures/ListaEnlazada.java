package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import model.logic.UBERTrip;

//import model.logic.UBERTrip;


public class ListaEnlazada<K extends Comparable<K>> implements IListaEnlazada<K>

{


	//------------
	//ATRIBUTOS
	//-----------
	private NodoListaEnlazada primero;

	private K elementos[];

	private int size;




	//------------
	//CONSTRUCTOR 
	//-----------
	public ListaEnlazada(  )
	{
		primero = null;
		size = 0;
	}



	//------------
	//METODOS
	//-----------

	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#size()
	 */
	public int size()
	{
		return size;
	}


	/*
	 * 
	 */
	public ArrayList<K> darDatos( )
	{
		ArrayList<K> datos = new ArrayList<K>( );
		NodoListaEnlazada<K> actual = primero;
		while( actual != null )
		{
			datos.add( (K) actual );
			actual = actual.darSiguiente( );
		}
		return datos;
	}


	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#agregar(java.lang.Comparable)
	 */
	public void agregar(K elemento)
	{

		NodoListaEnlazada<K> nuevo = new NodoListaEnlazada<K>(elemento, null);

		if (primero==null)
		{
			//Lista vacia
			primero = nuevo;
			size++;

		}
		else if(primero.compareTo(elemento)> 0 )
		{
			nuevo.cambiarSiguiente(primero);
			primero= nuevo;
			size++;
			
		}
		else
		{
			NodoListaEnlazada<K> NodoSi = primero; 

			while(NodoSi.darSiguiente()!=null )
			{
				if(NodoSi.darSiguiente().compareTo(elemento) >0 )
				{
					nuevo.cambiarSiguiente(NodoSi.darSiguiente());
					NodoSi.cambiarSiguiente(nuevo);
					size++;
				}

				NodoSi= NodoSi.darSiguiente();
			}
			NodoSi.cambiarSiguiente(nuevo);
			size++;
		}
	}




	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#eliminar(java.lang.Comparable)
	 */
	public K eliminar(K dato)
	{
		K elementoEliminado=null;

		if(primero!=null)
		{

			NodoListaEnlazada<K> nodoSi = primero; 

			if(nodoSi.obtenerDato().equals(dato))
			{
				if(nodoSi.darSiguiente()==null)
				{
					primero=null;
				}
				else
				{
					primero= nodoSi.darSiguiente();
				}
				elementoEliminado = nodoSi.obtenerDato();
				size--;
			}

			else
			{
				while(nodoSi.darSiguiente()!=null )
				{
					if(nodoSi.darSiguiente().obtenerDato().equals(dato))
					{
						nodoSi.cambiarSiguiente(nodoSi.darSiguiente().darSiguiente());
						size--;
						elementoEliminado = nodoSi.obtenerDato();
					}
					else if(nodoSi.darSiguiente().darSiguiente()==null && nodoSi.darSiguiente().obtenerDato().equals(dato)) 
					{
						nodoSi.cambiarSiguiente(null);
						size--;
						elementoEliminado = nodoSi.obtenerDato();
					}

					nodoSi = nodoSi.darSiguiente();
				}
			}
		}
		return elementoEliminado;

	}



	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#remplazar(java.lang.Comparable, java.lang.Comparable)
	 */
	@SuppressWarnings("unchecked")
	public void remplazar(K datoact, K nuevoele  )
	{
		if(primero!=null)
		{
			NodoListaEnlazada<K> nodoSi = primero; 

			while(!nodoSi.obtenerDato().equals(datoact) )
			{
				nodoSi= nodoSi.darSiguiente();
			}
			if(nodoSi != null)
				nodoSi.cambiarDato(nuevoele);
		}
	}



	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#buscar(int)
	 */
	@SuppressWarnings("unchecked")
	public K buscar(int pos )
	{
		K eleBuscado = null;

		if(primero!=null && pos>=0 && pos<size)
		{
			if(pos==0)
			{
				eleBuscado = (K) primero.obtenerDato();
			}
			else
			{
				int contador = 0;
				NodoListaEnlazada<K> nodoSi = primero; 

				while(nodoSi.darSiguiente()!=null && contador!=pos)
				{
					nodoSi=nodoSi.darSiguiente();
					contador++;
				}
				eleBuscado= (K) nodoSi.obtenerDato();
			}

		}

		return eleBuscado ;
	}



	public K darPrimero()
	{
		return ((K) primero);
	}

	
	

	public K darUltimo()
	{
		K aux = null;

		NodoListaEnlazada<K> nodoSi = primero; 

		while(nodoSi.darSiguiente()!=null)
		{
			nodoSi=nodoSi.darSiguiente();
			
		}
		aux = (K) nodoSi;
		
		
		return (K) primero;
	}


	
	
	

	public K [] toArrayFacts() 
	{
		
		
		 K[] arreglo = (K[]) new Object[size];
		 
		return arreglo;
	}








}