package model.data_structures;

public class NodoListaEnlazada <K extends Comparable<K>>
{
	
	
	
	private K elemento ;
	
	private NodoListaEnlazada<K> siguiente;
	
	
	
	
	/**
	 * 
	 * @param elemento
	 * @param siguiente
	 */
	public NodoListaEnlazada(K elemento, NodoListaEnlazada<K> siguiente)
	{
		this.elemento = elemento;
		
		this.siguiente = siguiente;
	}

	
	/**
	 * 
	 * @return
	 */
	public NodoListaEnlazada<K> darSiguiente()
	{
		return siguiente;
	}
	
	
	/**
	 * 
	 * @param sigNode
	 */
	public void cambiarSiguiente(NodoListaEnlazada<K> sigNode)
	{
		siguiente = sigNode;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public K obtenerDato()
	{
		return elemento;
	}
	
	/**
	 * 
	 * @param dato
	 */
	public void cambiarDato(K dato)
	{
		elemento = dato;
	}
	
	
	/**
	 * 
	 * @param dato
	 * @return
	 */
	public int compareTo(K dato) 
	{

		if(elemento.compareTo(dato)>0 ){
			return 1;
		}
		else if (elemento.compareTo(dato)==0){
			return 0;
		}
		else{
			return -1;
		}
	}
}
