package model.data_structures;

public interface IListaEnlazada <K extends Comparable<K>>
{

	int size ();

	void agregar(K dato);

	K eliminar(K dato);

	void remplazar(K dato, K dato2);

	K buscar (int pos);
	
	K darPrimero();
	
	K darUltimo();
	

	

}
