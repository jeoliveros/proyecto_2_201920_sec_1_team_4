package model.data_structures;



public class Nodo <Key extends Comparable<Key>, Value>
{

	//Nodo actual, es decir en el nodo que se va a trabajar
	Nodo nodoActual;
	//Representa la llave del nodo
	Key llave;
	// Representa el valor del nodo 
	Value valor;
	//Representa el nodo a la derecha del actual
	Nodo izq, dere;
	//Numero de nodos en el subArbol
	int N;
	//Color del enlazado entre el padre del nodoActual y el nodo.
	boolean color;
	ArbolBalanceadoRojoNegro arbolBalanceado;
	/**
	 * Metodo constructor
	 */
	public Nodo( Key pLlave, Value pVal, boolean pColor, int pNumeroDeNodos) {

		llave= pLlave;
		valor=pVal;
		color= pColor;
		N= pNumeroDeNodos;
	}

	public Key darLlave() {
		return llave;
	}
	
	/**
	 * Se da el nodo a la izquierda del actual
	 * 
	 */
	public Nodo NodoIzquierda(){
		return izq;
	}
	/**
	 * Se da el nodo a la derecha del actual
	 */
	public Nodo NodoDerecha() {
		return dere;
	}
	/**
	 * Se da la llave del nodo pasado por parametro
	 * @param pNodo
	 * @return
	 */
	public Key darLlaveCorrespondienteNodo(Nodo pNodo) {
		if(nodoActual==pNodo)
			return llave;
		return null;
	}
	 // the key of rank k in the subtree rooted at x
    private Nodo select(Nodo x, int k) {
        // assert x != null;
        // assert k >= 0 && k < size(x);
        int t = arbolBalanceado.size(izq); 
        if      (t > k) return select(x.izq,  k); 
        else if (t < k) return select(x.dere, k-t-1); 
        else            return x; 
    } 
	
	  public Key select(int k) {
	        if (k < 0 || k >= size()) {
	            throw new IllegalArgumentException("argument to select() is invalid: " + k);
	        }
	        Nodo x = select(nodoActual, k);
	        return llave;
	    }
	/**Metodo que se encarga de verificar si tiene mas hijos el nodo actual
	 * si es el caso va sumando esta 
	 */
	public int size() {

		if(izq==null && dere==null) {
			return 1;
		}
		else
		{
			int pesoIzq =0;
			int pesoDer =0;
			if(izq!=null) {
				pesoIzq = izq.size();
			}
			if(dere!=null) {
				pesoDer= dere.size();
			}
			return (1+ pesoIzq + pesoDer);
		}
	}
	

	//Se devuelve el valor del nodo seleccion 
	public Value darValor() {
		return valor;
	}
	
	
	int altura(Nodo node)  
	{  
	    if (node == null)  
	        return 0;  
	    else
	    {  
	        /* compute the depth of each subtree */
	        int lDepth = altura(node.izq);  
	        int rDepth = altura(node.dere);  
	      
	        /* use the larger one */
	        if (lDepth > rDepth)  
	            return(lDepth + 1);  
	        else return(rDepth + 1);  
	    }  
	}  
	
	
	
	
	
}
