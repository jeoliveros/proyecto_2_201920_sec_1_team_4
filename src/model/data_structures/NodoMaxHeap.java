package model.data_structures;



public class NodoMaxHeap <T extends Comparable<T>>
{
	
	
	
	private T elemento ;
	
	//private NodoMaxHeap<T> siguiente;
	
	
	
	
	/**
	 * 
	 * @param elemento
	 * @param siguiente
	 */
	public NodoMaxHeap(T elemento) //NodoMaxHeap<T> siguiente)
	{
		this.elemento = elemento;
		
	//	this.siguiente = siguiente;
	}

	
	/**
	 * 
	 * @return
	 */
//	public NodoMaxHeap<T> darSiguiente()
//	{
//		return siguiente;
//	}
	
	
	/**
	 * 
	 * @param sigNode
	 */
//	public void cambiarSiguiente(NodoMaxHeap<T> sigNode)
//	{
//		siguiente = sigNode;
//	}
	
	
	/**
	 * 
	 * @return
	 */
	public T obtenerDato()
	{
		return elemento;
	}
	
	/**
	 * 
	 * @param dato
	 */
	public void cambiarDato(T dato)
	{
		elemento = dato;
	}
	
	
	/**
	 * 
	 * @param dato
	 * @return
	 */
	public int compareto(T dato) 
	{

		if(elemento.compareTo(dato)>0 ){
			return 1;
		}
		else if (elemento.compareTo(dato)==0){
			return 0;
		}
		else{
			return -1;
		}
	}
}
