package model.data_structures;

import java.util.Iterator;
import java.lang.Math;
import java.lang.reflect.Array;
import java.util.NoSuchElementException;


public class MaxHeapCP  <T extends Comparable<T>>  
{
	
	private T[] arregloNodos;
	private int  N ;
	

	
	
	public MaxHeapCP(int maxN)//Class<T> class1
	{
		
		
		arregloNodos = (T[]) new Comparable[maxN];

		
		
		
	}
	
	
	public T  darElemento(int i)
	{
		T retornar = arregloNodos[i];
		
		return retornar;
		
	}
	

	public boolean isEmpty() 
	{
		return N == 0;
	}
	
	public int size()
	{
		return N; 
	}
	
	
	private void exch(int i, int j)
	{  
		T t = arregloNodos[i]; arregloNodos[i] = arregloNodos[j]; arregloNodos[j] = t; 
	}
	
	
	
	private boolean less(T i, T j)
	{ 
		boolean resp = false;
		if (i.compareTo(j) > 0)
			
		{
			resp =true;
		}
		return resp; 
	}
	
	
	public void agregar(T Nodo)
	{
		
		System.out.println(Nodo);
		arregloNodos[++N] = Nodo;
		swim(N);
		
		
//		N++;
//		System.out.println(N);
//		
//		T nuevo = Nodo;
//		
//		
//		if(N == 1)
//		{
//			
//		 arregloNodos[N] = nuevo;
//			
//			
//		}
//		
//		else 
//		{
//			arregloNodos[N] = nuevo;
//			
//			//elementos[N] = (T) n;
//			
//		}
//		swim(N);
		
	}
	
	
	public void swim(int posicion) 
	{
		
		double var = (posicion) / 2;
		
		int papa = (int) Math.floor(var);
		
		T Nodopapa =  arregloNodos[papa];
		T Nodohijo = arregloNodos[posicion];
		
		boolean seguir = less(Nodohijo, Nodopapa);
		
		while( posicion > 1 &&  seguir== true )
		{
			exch(papa, posicion);
			posicion =  (int) Math.floor(posicion/2);
		}
	}
	

	public T eliminarMax()
	{
		if (isEmpty()) return null;

		
		T maxele = arregloNodos[1];
		
		exch(1,N--);

		arregloNodos[N]= null;
		
		sink(1);

		return maxele;
	}
	

	
	public void sink(int posicion)
	{
		boolean termino = false;

		
		while (!termino)
		{
			int izquierda = 2 * posicion ; 
			int derecha = 2 * posicion + 1; 
			
			int mayor = izquierda; 

			T NodoDer = arregloNodos[izquierda];
			T NodoIzq = arregloNodos[derecha];
			T Nodopos = arregloNodos[posicion];

			if (derecha < N &&  less(NodoIzq, NodoDer)== true )
				{
				mayor = derecha;
				}
			
			if(izquierda>= N || less(NodoIzq, Nodopos)==true)
			{
				termino = true; break;
			}			
			
			exch(mayor, posicion);
			posicion= mayor;	
		}
	}

}