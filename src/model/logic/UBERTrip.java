package model.logic;

public class UBERTrip implements Comparable<UBERTrip>
{

	
private static int sourceId ;
	
	private static int destinID;
	
	private static int hora;
	
	private  static double meanTravelTime;
	
	private static double standardDeviationTravelTime;
	
	private static double geometricMeanTravelTime;
	
	private static double geometricStandardDeviationTravelTime;
	
	public UBERTrip(int pSource, int pdestinID, int phora, double pmeanTravelTime, double pstandardDeviationTravelTime, double pgeometricMeanTravelTime, double pgeometricStandardDeviationTravelTime   )
	{
		sourceId= pSource;
		destinID= pdestinID;
		hora = phora;
		meanTravelTime=pmeanTravelTime;
		standardDeviationTravelTime = pstandardDeviationTravelTime;
		geometricMeanTravelTime= pgeometricMeanTravelTime;
		geometricStandardDeviationTravelTime = pgeometricStandardDeviationTravelTime;
	}

	
	public int darsourceId() 
	{
		return sourceId;
	}
	
	
	
	public  int dardestinID() 
	{
		return destinID;
	}
	
	
	public int darHora() 
	{
		return hora;
	}
	
	
	public  double darmeanTravelTime() 
	{  
		return meanTravelTime;
	}
	
	
	public  double darstandardDeviationTravelTime() 
	{
		return standardDeviationTravelTime;
	}
	
	
	public  double dargeometricMeanTravelTime() 
	{
		return geometricMeanTravelTime;
	}
	
	
	public  double dargeometricStandardDeviationTravelTime() 
	{
		return geometricStandardDeviationTravelTime;
	}

	
	
	
	
	//compara por mean travel time si no entonces standart
	//-1 si o.dar es mayor
	//1 si mean travel es mayor 

	public int compareTo(UBERTrip o) 
	{
		
		int aux =0;
		
		if (o.darmeanTravelTime() > meanTravelTime)
		{
			aux = -1 ;
		}
		else if (o.darmeanTravelTime() < meanTravelTime)
		{
			aux =1;
		}
		else if (o.darmeanTravelTime() == meanTravelTime)
		{
			if (o.dargeometricStandardDeviationTravelTime() > standardDeviationTravelTime )
			{
				aux = -1;
			}
			else if (o.dargeometricStandardDeviationTravelTime() < standardDeviationTravelTime)
			{
				aux =1;
			}
				
		}
		
		return aux;
	}

}
