package model.logic;

public class viajeUberDia implements Comparable<viajeUberDia>
{

	private  int sourceId ;
	
	private  int destinID;
	
	private  int dia;
	
	private   double meanTravelTime;
	
	private  double standardDeviationTravelTime;
	
	private  double geometricMeanTravelTime;
	
	private  double geometricStandardDeviationTravelTime;
	
	public viajeUberDia(int pSource, int pdestinID, int  pdia, double pmeanTravelTime, double pstandardDeviationTravelTime, double pgeometricMeanTravelTime, double pgeometricStandardDeviationTravelTime   )
	{
		sourceId= pSource;
		destinID= pdestinID;
		dia = pdia;
		meanTravelTime=pmeanTravelTime;
		standardDeviationTravelTime = pstandardDeviationTravelTime;
		geometricMeanTravelTime= pgeometricMeanTravelTime;
		geometricStandardDeviationTravelTime = pgeometricStandardDeviationTravelTime;
	}

	
	public  int darsourceId() 
	{
		return sourceId;
	}
	
	
	public  int dardestinID() 
	{
		return destinID;
	}
	
	
	public  int dardia() 
	{
		return dia;
	}
	
	
	public double darmeanTravelTime() 
	{
		return meanTravelTime;
	}
	
	
	public  double darstandardDeviationTravelTime() 
	{
		return standardDeviationTravelTime;
	}
	
	
	public  double dargeometricMeanTravelTime() 
	{
		return geometricMeanTravelTime;
	}
	
	
	public  double dargeometricStandardDeviationTravelTime() 
	{
		return geometricStandardDeviationTravelTime;
	}


	@Override
	public int compareTo(viajeUberDia o) 
	{
		
//		int aux =0;
//		
//		if (destinID > o.dardestinID() )
//		{
//			aux = -1 ;
//		}
//		else if (destinID < o.dardestinID() )
//		{
//			aux =1;
//		}
//		
		
		return 0;
	}

}
