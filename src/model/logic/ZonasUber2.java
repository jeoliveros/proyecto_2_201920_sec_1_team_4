package model.logic;

import com.google.gson.*;

import model.data_structures.ArbolBalanceadoRojoNegro;
import model.data_structures.MaxHeapCP;
import model.data_structures.Punto;
import model.data_structures.TablaHashSC;




public class ZonasUber2 implements Comparable<ZonasUber2>
{

	private JsonArray arreglopuntos;
	
	
	private  int cartodb_id ;

	private  int scacodigo;

	private  int scatipo;

	private   String  scanombre;

	private  double shape_leng;

	private  double shape_area;

	private  int MOVEMENT_ID;
	
	private String DISPLAY_NAME;
	

	public ZonasUber2(JsonArray arreglosConPuntos ,int pcartodb_id, int pscacodigo, int pscatipo, String pscanombre, double pshape_leng, double pshape_area, int pMOVEMENT_ID   )
	{
		arreglopuntos = arreglosConPuntos;
		
		cartodb_id= pcartodb_id;
		scacodigo= pscacodigo;
		scatipo = pscatipo;
		scanombre = pscanombre;
		shape_leng = pshape_leng;
		shape_area= pshape_area;
		MOVEMENT_ID = pMOVEMENT_ID;
		DISPLAY_NAME= (pscanombre +","+ pscacodigo +"("+ pMOVEMENT_ID+")" );
		
	}


	





	public JsonArray getArreglopuntos() 
	{
		return arreglopuntos;
	}



	public void setArreglopuntos(JsonArray arreglopuntos) 
	{
		this.arreglopuntos = arreglopuntos;
	}


	public int getCartodb_id() {
		return cartodb_id;
	}


	public void setCartodb_id(int cartodb_id) {
		this.cartodb_id = cartodb_id;
	}


	public int getScacodigo() {
		return scacodigo;
	}


	public void setScacodigo(int scacodigo) {
		this.scacodigo = scacodigo;
	}


	public int getScatipo() {
		return scatipo;
	}



	public void setScatipo(int scatipo) {
		this.scatipo = scatipo;
	}


	public String getScanombre() {
		return scanombre;
	}

	public void setScanombre(String scanombre) {
		this.scanombre = scanombre;
	}


	public double getShape_leng() {
		return shape_leng;
	}



	public void setShape_leng(double shape_leng) {
		this.shape_leng = shape_leng;
	}



	public double getShape_area() {
		return shape_area;
	}



	public void setShape_area(double shape_area) {
		this.shape_area = shape_area;
	}




	public int getMOVEMENT_ID() {
		return MOVEMENT_ID;
	}



	public void setMOVEMENT_ID(int mOVEMENT_ID) {
		MOVEMENT_ID = mOVEMENT_ID;
	}


	public String getDISPLAY_NAME() {
		return DISPLAY_NAME;
	}


	//compara por mean travel time si no entonces standart
	//-1 si o.dar es mayor
	//1 si numeropuntos es mayor 
	@Override
	public int compareTo(ZonasUber2 o) 
	{
		int resp;
		
	
		
		
		int numeropuntos = arreglopuntos.size();
		int numpuntos_o = o.getArreglopuntos().size();
		
		
		
		if (numeropuntos >  numpuntos_o  )
		{
			resp = 1;
		}
		if (numeropuntos <  numpuntos_o  )
		{
			resp = -1;
		}
		else
		{
			resp=0;			
		}
	
		return resp;
	}
	
	

	
	
	
	
	
	
	
	
	

}
