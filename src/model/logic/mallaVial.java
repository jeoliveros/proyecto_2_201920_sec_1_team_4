package model.logic;

public class mallaVial 
{

	private static int IDNodo;
	
	private static double longitud;
	
	private static double latitud;
	
	
	public mallaVial(int pidNodo, double pLongitud, double pLatitud   )
	{
		
		IDNodo= pidNodo;
	
		longitud = pLongitud;
	
		latitud = pLatitud;
	}
	
	
	
	public  int darIdNodo() 
	{
		return IDNodo;
	}
	
	public  double darlongitud() 
	{
		return longitud;
	}
	
	
	public  double darlatitud() 
	{
		return latitud;
	}
	
}
