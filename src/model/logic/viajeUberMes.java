package model.logic;

public class viajeUberMes implements Comparable<viajeUberMes>
{
private static int sourceId ;
	
	private  int destinID;
	
	private  int mes;
	
	private   double meanTravelTime;
	
	private  double standardDeviationTravelTime;
	
	private  double geometricMeanTravelTime;
	
	private  double geometricStandardDeviationTravelTime;
	
	public viajeUberMes(int pSource, int pdestinID, int  pmes, double pmeanTravelTime, double pstandardDeviationTravelTime, double pgeometricMeanTravelTime, double pgeometricStandardDeviationTravelTime   )
	{
		sourceId= pSource;
		destinID= pdestinID;
		mes = pmes;
		meanTravelTime=pmeanTravelTime;
		standardDeviationTravelTime = pstandardDeviationTravelTime;
		geometricMeanTravelTime= pgeometricMeanTravelTime;
		geometricStandardDeviationTravelTime = pgeometricStandardDeviationTravelTime;
	}

	
	public  int darsourceId() 
	{
		return sourceId;
	}
	
	
	public  int dardestinID() 
	{
		return destinID;
	}
	
	
	public  int darMes() 
	{
		return mes;
	}
	
	
	public  double darmeanTravelTime() 
	{
		return meanTravelTime;
	}
	
	
	public  double darstandardDeviationTravelTime() 
	{
		return standardDeviationTravelTime;
	}
	
	
	public  double dargeometricMeanTravelTime() 
	{
		return geometricMeanTravelTime;
	}
	
	
	public  double dargeometricStandardDeviationTravelTime() 
	{
		return geometricStandardDeviationTravelTime;
	}


	@Override
	public int compareTo(viajeUberMes o) {
		
		return 0;
	}

}
