package model.logic;

public class viajeUberHora implements Comparable<viajeUberHora>
{

	
	private int sourceId ;
	
	private int destinID;
	
	private int Hod;
	
	private double meanTravelTime;
	
	private double standardDeviationTravelTime;
	
	private double geometricMeanTravelTime;
	
	private double geometricStandardDeviationTravelTime;
	
	public viajeUberHora(int pSource, int pdestinID, int  pHod, double pmeanTravelTime, double pstandardDeviationTravelTime, double pgeometricMeanTravelTime, double pgeometricStandardDeviationTravelTime   )
	{
		sourceId= pSource;
		destinID= pdestinID;
		Hod = pHod;
		meanTravelTime=pmeanTravelTime;
		standardDeviationTravelTime = pstandardDeviationTravelTime;
		geometricMeanTravelTime= pgeometricMeanTravelTime;
		geometricStandardDeviationTravelTime = pgeometricStandardDeviationTravelTime;
	}

	
	public int darsourceId() 
	{
		return sourceId;
	}
	
	
	public int dardestinID() 
	{
		return destinID;
	}
	
	
	public int darHod() 
	{
		return Hod;
	}
	
	
	public double darmeanTravelTime() 
	{
		return meanTravelTime;
	}
	
	
	public double darstandardDeviationTravelTime() 
	{
		return standardDeviationTravelTime;
	}
	
	
	public double dargeometricMeanTravelTime() 
	{
		return geometricMeanTravelTime;
	}
	
	
	public double dargeometricStandardDeviationTravelTime() 
	{
		return geometricStandardDeviationTravelTime;
	}


	@Override
	public int compareTo(viajeUberHora o) 
	{
		
		return 0;
	}
	
}
