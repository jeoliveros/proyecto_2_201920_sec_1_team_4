package model.logic;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.PriorityQueue;

import org.json.JSONArray;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import model.data_structures.*;

public class RequerimientosA {


	TablaHashSC<String, Integer> tabla_n_letras = new TablaHashSC<String, Integer>(29);
	/**
	 * Este metodo se encarga de resolver el requerimiento 1A
	 * @param N
	 * @param pArbolBalanceado
	 * @return 
	 */
	public String[] requerimiento1A(int N, ArbolBalanceadoRojoNegro<Integer, ZonasUber> pArbolBalanceado) {
		String[] arreglo= new String[30];  

		/*
		 * 1. recorrer el arbol
		 * 1.1 como el metodo de recorrer un arbol devuelve una tabla recorremos la tabla
		 * 1.1 Hacer un while que verifique cual es el mayor de los valores
		 * y vaya agregando de mayor a menor
		 * 2. Se van agregando al arreglo de mayor a menor 
		 * 3. Se devuelve un arreglo de strings que los contiene organizados
		 */
		//variable que ayufa a encontrar el mayor

		tabla_n_letras = recorrerArbol(pArbolBalanceado);
		int size = tabla_n_letras.size(); //El tama�o original de la tabla
		for (int i=0; i < size; i++) //Se quieren hacer size recorridos de la tabla
		{
			Iterator<String> letras = tabla_n_letras.keys().iterator(); //Iterando las llaves
			int mayor = 0;				//Variables de control
			String letraMayor = "";		//para buscar el mayor
			while(letras.hasNext())		//Se hacen iteraciones hasta que no queden letras
			{
				String letraActual = letras.next();
				int numeroDeLetras =   tabla_n_letras.getSet(letraActual);
				if(numeroDeLetras > mayor)	//Se encontr� un nuevo mayor
				{
					mayor = numeroDeLetras;
					letraMayor = letraActual;
				}
			}
			tabla_n_letras.deleteSet(letraMayor);	//Se elimina el mayor para poder buscar el siguiente mayor
			arreglo[i] = letraMayor;				//Se agrega el nuevo mayor a la respuesta
		}

		return arreglo;
	}
	/**
	 * Este metodo es un metodo auxiliar para el requerimiento1A 
	 * Se encarga de recorrer el arbol 
	 * @param pArbolBalanceado
	 * @return
	 */
	public TablaHashSC<String, Integer> recorrerArbol(ArbolBalanceadoRojoNegro<Integer, ZonasUber> pArbolBalanceado){

		/*1.1 Recorrer el arbol ir uno a uno por sus elementos
		 * 1.2 Cada uno de las zonas le sacamos el nombre
		 * 1.3 Sacamos la primera letra de ese nombre
		 * 1.4 La letra es una key
		 * 1.5 la repeticion de la letra es el valor
		 * 1.6 Se mete esta info en una tabla de hash
		 * 2. retornar la tabla de hash generada
		 */
		int repeticionLetra =0;

		for (int i = 0; i < pArbolBalanceado.size(); i++) {

			String nombre = pArbolBalanceado.get(i).getScanombre();
			String primeraLetra = nombre.substring(0,1);
			repeticionLetra = tabla_n_letras.getSet(primeraLetra) + 1;
			tabla_n_letras.putInSet(primeraLetra, repeticionLetra);
		}
		return  tabla_n_letras;
	}
	/**
	 * 
	 * @param pArbolBalanceado
	 * @param pLatitud
	 * @param pLongitud
	 * @return
	 */
	public ArbolBalanceadoRojoNegro<Integer, Object[]> numeroDeNodos (ArbolBalanceadoRojoNegro<Integer, ZonasUber> pArbolBalanceado,double pLatitud, double pLongitud) {
		/*1.1 Recorrer el arbol ir uno a uno por sus elementos
		 * 1.2 Cada uno de sus elementos sacarle el arreglo de puntos que contiene
		 * la longitud y latitud de la zona
		 * 1.3 Hace comparcion de la longitud y la latitud dada por parametro con 
		 * con la longitud y la latitud recibida de cada lugar
		 * 1.4 Si cumplen con la condicion lo metemos a un arreglo
		 * 2. Iteramos el arreglo para saber cuantos elementso hay (contamos cuantos hay con un contador)
		 * 3 retornamos el contador 
		 */
		ArbolBalanceadoRojoNegro<Integer, Object[]> arbolAR2 = new ArbolBalanceadoRojoNegro<Integer, Object[]>();
		Object arreglo[]=  new Object[2];
		int respuesta = 0;

		String pLatEntera = String.valueOf(pLatitud).split(".")[0];
		String pLatDecimal = String.valueOf(pLatitud).split(".")[1].substring(0,3);
		String pLongEntera = String.valueOf(pLongitud).split(".")[0];
		String pLongDecimal = String.valueOf(pLongitud).split(".")[1].substring(0,3);

		for (int i = 0; i < pArbolBalanceado.size(); i++) {
			JsonArray Zona = pArbolBalanceado.get(i).getArreglopuntos();

			for (int j = 0; j < Zona.size(); j++) {
				String latitudActual = Zona.get(i).getAsJsonArray().get(0).getAsString();
				String longActual = Zona.get(i).getAsJsonArray().get(1).getAsString();

				String latEntera = latitudActual.split(".")[0];
				String latDecimal = latitudActual.split(".")[1].substring(0,3);

				String longEntera = longActual.split(".")[0];
				String longDecimal = longActual.split(".")[1].substring(0,3);

				//Comparacion latitud
				boolean compararLatEntera = latEntera.equals(pLatEntera);
				boolean compararLatDecimal = latDecimal.equals(pLatDecimal);

				//Comparacion longitud
				boolean compararLongEntera = longEntera.equals(pLongEntera);
				boolean compararLongDecimal = longDecimal.equals(pLongDecimal);

				if(compararLatEntera && compararLatDecimal && compararLongEntera && compararLongDecimal)
				{
					/*
					 * - Nombre de la zona
					 * - Los valores del nodo (lat, long)
					 * - sumar a la cantidad
					 */


					String nombreZona=pArbolBalanceado.get(i).getScanombre();
					JsonArray LatitudYLongitud= Zona.get(i).getAsJsonArray();
					arreglo[0] = nombreZona;
					arreglo[1]= LatitudYLongitud;

					arbolAR2.put(respuesta, arreglo);

					respuesta++;

				}


			}
		}

		return arbolAR2;

	}
	/**
	 * Conecta todos los metodos que nos permiten responder al R2A
	 * @return el String que incluye el numero de nodos que cumplen
	 * con la condicion y de cada nodo su latitud, longitud y el 
	 * nombre de la zona a la que pertenece 
	 */
	public String nodosDelimitantesDeZonaLocalizacionGeometrica() {
		String respuesta="El numero de nodos que cumplen con la latitud y la longitud dada son:";
		return respuesta;
	}

	public MaxHeapCP<viajeUberMes> requerimientoParte3A(MaxHeapCP<viajeUberMes> pColaDePrioridad,int pLimite_bajo, int pLimite_alto, int nViajesOrdenados){
		MaxHeapCP<viajeUberMes> cumpleParametroTiempoPromedio= new MaxHeapCP<viajeUberMes>(nViajesOrdenados);
		/*1.Creamos la MaxHeap donde vamos guardar los que cumplen con los requerimientos 
		 * 2. iteramos la colaDePrioridad 
		 * 2.1 en la iteracion visualizamos si el valor actual esta entre el rango
		 * del limite bajo y el limite alto
		 * 2.2 Si cumple lo metemos en la Cola de prioridad nueva 
		 * 
		 */
		for (int i = 0; i < pColaDePrioridad.size(); i++) {
			double tiempoPromedio =pColaDePrioridad.darElemento(i).darmeanTravelTime();
			if(tiempoPromedio< pLimite_alto && tiempoPromedio> pLimite_bajo) {
				cumpleParametroTiempoPromedio.agregar(pColaDePrioridad.darElemento(i));
			}
		}



		return cumpleParametroTiempoPromedio;
	}
	/**
	 * Este metodo se encarga de resolver el corazon del requerimientos 1C
	 * @param pTablaHASH esta tablaHash contiene la lectura de los viajesUberMes
	 * @param pZonadada
	 * @param pZonaDestino
	 * @return
	 */
	public ListaEnlazada <viajeUberHora> requerimientosParte1C(TablaHashSC<Integer, viajeUberHora> pTablaHash,int pZonadada,int pHoraDada){
		ListaEnlazada<viajeUberHora> listaCumplePrametros= new ListaEnlazada<viajeUberHora>();
		/*1. Se crea la listaEnlazada donde se van a guardar los viajes 
		 * que cumplan con las condiciones escritas en el parametro
		 *2.Se empieza a iterar la tablaHash 
		 *2.1 Con un if se verifica si cumple con las condiciones que estan
		 *en parametro
		 *2.2 Si cumple se guarda en la lista enlazada
		 * 
		 */


		Iterator iterador = pTablaHash.keys().iterator();

		while(iterador.hasNext()) {

			int i = ((Integer) ((HashNode) iterador.next()).dar_Key()).intValue();
			//int i = iterador.next().intValue();

			if(pTablaHash.getSet(i)!=null&&pTablaHash.getSet(i).darsourceId()== pZonadada && pTablaHash.getSet(i).darHod()==pHoraDada ) {
				viajeUberHora cumple = pTablaHash.getSet(i);
				listaCumplePrametros.agregar(cumple);
			}
		}
		return listaCumplePrametros;
	}
	/**
	 * Metodo que se encarga de resolver el requerimiento 2C
	 * @param pTablaHash
	 * @param pNombreZona
	 * @param pHoraInicial
	 * @param pHoraFinal
	 * @return
	 */
	public ListaEnlazada<viajeUberHora> requerimientoParteC2(TablaHashSC<Integer, viajeUberHora> pTablaHash,int pNombreZona, int pHoraFinal, int pHoraInicial) {
		/*1. Recorremos el arreglo donde se cargaron los datos
		 * 2. Despues de visualizarlos a cada uno le preguntamos
		 * si cumple los requerimientos propuestos. Es decir si
		 * el nodo alctual tiene el mismo nombre de zona, la misma
		 * hora inicial , la misma hora final o esta entre los 
		 * parametros se agrega a una lista enlazada.
		 * 3.Se retorna la listaEnlazada que se lleno con las vereficaciones propuestas
		 * 
		 * 
		 */
		ListaEnlazada<viajeUberHora> listaEnlazada = new ListaEnlazada<viajeUberHora>();
		Iterator iterador = pTablaHash.keys().iterator();

		System.out.println("Recuper� el iterador ");													//DEBUG
		System.out.println("Zona: " + pNombreZona + " - Hora: " + pHoraInicial + "-" + pHoraFinal);		//DEBUG
		while(iterador.hasNext()) {
			int i = ((Integer) ((HashNode) iterador.next()).dar_Key()).intValue();
			
			// Creamos una variable para guardar el valor que nos da la tabla
			viajeUberHora verificar =  pTablaHash.getSet(i);
			// Verificar que el nombre de la Zona es el mismo al dado por parametro
			if(verificar!=null &&verificar.dardestinID()== pNombreZona) {
				if(verificar.darHod()< pHoraFinal && verificar.darHod()> pHoraInicial ) {
					listaEnlazada.agregar(verificar);
				}
			}

		}
		return listaEnlazada;
	}
}

