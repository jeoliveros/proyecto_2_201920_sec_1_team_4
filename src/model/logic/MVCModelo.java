package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.*;
//import com.sun.tools.classfile.StackMapTable_attribute.verification_type_info;

import model.data_structures.ArbolBalanceadoRojoNegro;
import model.data_structures.ListaEnlazada;
import model.data_structures.MaxHeapCP;
import model.data_structures.TablaHashSC;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo <T extends Comparable<T>>
{

	public final static String MES1 = "./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv";
	public final static String HORA1 = "./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv";
	public final static String SEMANA1 = "./data/bogota-cadastral-2018-1-WeeklyAggregate.csv";

	public final static String MES2 = "./data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv";
	public final static String HORA2 = "./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv";
	public final static String SEMANA2 = "./data/bogota-cadastral-2018-2-WeeklyAggregate.csv";

	public final static String MES3 = "./data/bogota-cadastral-2018-3-All-MonthlyAggregate.csv";
	public final static String HORA3 = "./data/bogota-cadastral-2018-3-All-HourlyAggregate.csv";
	public final static String SEMANA3 = "./data/bogota-cadastral-2018-3-WeeklyAggregate.csv";

	public final static String MES4 = "./data/bogota-cadastral-2018-4-All-MonthlyAggregate.csv";
	public final static String HORA4 = "./data/bogota-cadastral-2018-4-All-HourlyAggregate.csv";
	public final static String SEMANA4 = "./data/bogota-cadastral-2018-4-WeeklyAggregate.csv";


	public final static String DATOS_MALLA = "./data/Nodes_of_red_vial-wgs84_shp.txt";
	public final static String DATOS_JSON = "./data/bogota_cadastral.json";



	public final static String HORA_PRUEBA = "./data/ZHora1.txt";
	public final static String DATOS_MALLA_PRUEBA = "./data/ZmallaVial.txt";
	public final static String MESP_PRUEBA = "./data/ZMes1.txt";
	public final static String SEMANA_PRUEBA = "./data/ZSemana1.txt";



	/**
	 * Atributos del modelo del mundo
	 */
	//Conexion entre RequerimientosA y el MVCModelo

	public RequerimientosA reqA;

	private ArbolBalanceadoRojoNegro<Integer, ZonasUber> arbolbalanceado_JSON;

	private ArbolBalanceadoRojoNegro arbolBalanceado_R2A;

	private int tamano_tablaHashR3A;
	private TablaHashSC tablaHash_R3A;

	private int tamano_tablaHashR1B; 
	private TablaHashSC tablaHashR1B;

	private ArbolBalanceadoRojoNegro arbolbalanceadoMALLA_R2B;

	int tamano_PriorityQueu_R3B;
	private MaxHeapCP<viajeUberMes> PriorityQueu_R3B;



	private TablaHashSC<Integer, viajeUberHora> tablaHashHoras;

	private int tamano_PriorityQueu_R2C;
	private MaxHeapCP<viajeUberHora> PriorityQueu_R2C;


	private MaxHeapCP<ZonasUber2> priorityQueueZonaNumNodos_R3C;

	private ArbolBalanceadoRojoNegro arbolbalanceadoTablaAscii;



	//private ArbolBalanceadoRojoNegro<Integer, ZonaUber> arbolDeZonas;






	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		reqA = new RequerimientosA();
		
		System.out.println("Se construy� el reqA");
		
		//los rquerimientos de la parte A

		//ARCHIVO JSON


		//ARCHIVO JSON
		arbolbalanceado_JSON = new ArbolBalanceadoRojoNegro<>();
		
		//VIAJE UBER MES 
		tamano_tablaHashR3A=100;
		tablaHash_R3A = new TablaHashSC<>(tamano_tablaHashR3A);


		
		//los rquerimientos de la parte B

		//ARCHIVO JSON
		tamano_tablaHashR1B=100;
		tablaHashR1B = new TablaHashSC<>(tamano_tablaHashR1B);

		//ARCHIVO MALLA VIAL
		arbolbalanceadoMALLA_R2B = new ArbolBalanceadoRojoNegro<>();

		
		
		//ARCHIVO VIAJE UBER MES 
		tamano_PriorityQueu_R3B =100;
		PriorityQueu_R3B = new MaxHeapCP<viajeUberMes>(tamano_PriorityQueu_R3B);



		//los rquerimientos de la parte C

		// ARCHVIO VIAJE UBER HORA 
		tablaHashHoras = new TablaHashSC<Integer, viajeUberHora>(13);

		// ARCHVIO VIAJE UBER HORA 
		tamano_PriorityQueu_R2C=100;
		PriorityQueu_R2C = new MaxHeapCP<viajeUberHora>(tamano_PriorityQueu_R2C) ;

		// ARCHVIO JSON 
		priorityQueueZonaNumNodos_R3C = new MaxHeapCP<>(1161);

		// ARCHVIO VIAJE UBER HORA 
		arbolbalanceadoTablaAscii = new ArbolBalanceadoRojoNegro();


	}

	/**
	 * 
	 * @param TrimestreSeleccionado
	 * @return
	 */
	public String cargarDatosViajes() 
	{

		int lienasMes= 0;
		int lineasSemana= 0;
		int lineasHora = 0;

		int total = 0 ;




		lineasHora = CargarHora(HORA1, 1);
		lienasMes = CargarMes(MES1, 1);


		lineasHora = lineasHora + CargarHora(HORA2, 2);
		lienasMes = lienasMes + CargarMes(MES2, 2);	
		lineasHora = lineasHora + CargarHora(HORA3, 3);
		lienasMes = lienasMes + CargarMes(MES3, 3);
		lineasHora = lineasHora + CargarHora(HORA4, 4);
		lienasMes = lienasMes + CargarMes(MES4, 4);


		//		lineasSemana = CargarSemana(SEMANA_PRUEBA, 1) ;
		//		lineasSemana = lineasSemana + CargarSemana(SEMANA2, 2);
		//		lineasSemana = lineasSemana + CargarSemana(SEMANA3, 3);
		//		lineasSemana = lineasSemana + CargarSemana(SEMANA4, 4);



		total = lienasMes+lineasHora+lineasSemana;

		return ("El número de viajes que se cargaron de cada archivo CSV: "+  total);
	}




	/**
	 * 
	 * @param archivomes
	 * @param trimestre
	 * @return
	 */
	public int CargarMes( String archivomes, int trimestre) 
	{


		File archivo = new File(archivomes);

		FileReader lector;
		int totalViajes=0;



		try {


			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);

			String linea = bf.readLine();
			linea= bf.readLine();


			while(linea != null )
			{
				String[] datos = linea.split(",");

				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);



				viajeUberMes viaje = new viajeUberMes(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

				//ANADE A TABLA HASH
				String llave =( trimestre+"-"+ viaje.darsourceId()+"-"+viaje.dardestinID());




				//Usaremos la priorityQueu tanto para el R2B como para el R3A

				PriorityQueu_R3B.agregar(viaje);
				tamano_PriorityQueu_R3B++;


				System.out.println(totalViajes);

				linea = bf.readLine();
				totalViajes++;

			}


		}
		catch (Exception e) {
			e.printStackTrace();

		}

		return totalViajes;


	}

	/**
	 * 
	 * @param archivohora
	 * @param trimestre
	 * @return
	 */
	public int CargarHora(String archivohora, int trimestre )
	{

		int totalViajes=0;


		File archivo = new File(archivohora);

		FileReader lector;

		try {
			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);
			String linea = bf.readLine();
			linea= bf.readLine();


			while(linea != null )
			{
				String[] datos = linea.split(",");

				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);

				viajeUberHora viaje = new viajeUberHora(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);


				String llave =( trimestre+"-"+ viaje.darsourceId()+"-"+viaje.dardestinID());

				tablaHashHoras.putInSet(sourceid, viaje);

				PriorityQueu_R2C.agregar(viaje);
				tamano_PriorityQueu_R2C++;

				arbolbalanceadoTablaAscii.put(llave, viaje);


				System.out.println(totalViajes);

				linea = bf.readLine();
				totalViajes++;
			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}

		return totalViajes;

	}

	/**
	 * 
	 * @param archivosemana
	 * @param trimestre
	 * @return
	 */
	public int CargarSemana(String archivosemana ,int trimestre)
	{
		int totalViajes=0;
		File archivo = new File(archivosemana);
		FileReader lector;

		try {
			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);
			String linea = bf.readLine();
			linea= bf.readLine();

			while(linea != null ){
				String[] datos = linea.split(",");

				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);

				viajeUberMes viaje = new viajeUberMes(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

				String llave =( trimestre+"-"+ viaje.darsourceId()+"-"+viaje.dardestinID());


				linea = bf.readLine();
				totalViajes++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return totalViajes;
	}



	public int leerDatosJson()
	{
		int contador = 0;

		try {


			//Leemos el archivo
			FileReader archivo = new FileReader(DATOS_JSON);
			BufferedReader lector = new BufferedReader(archivo);


			JsonElement elementoPrincipal = new JsonParser().parse(lector);


			//obtenemos cada zaona
			JsonObject obj = elementoPrincipal.getAsJsonObject();


			JsonArray listafeatures = obj.getAsJsonArray("features");


			//Buscamos la informacion de cada zona
			for (int i = 0; i < listafeatures.size(); i++)
			{


				JsonObject ZONA = listafeatures.get(i).getAsJsonObject();

				if(ZONA == null)
					continue;


				JsonObject geometria = ZONA.getAsJsonObject("geometry");

				if(geometria == null)
					continue;

				JsonArray cordenadas = geometria.getAsJsonArray("coordinates");

				if(cordenadas == null)
					continue;

				//System.out.println("Alguno llega después del continue");

				cordenadas = cordenadas.get(0).getAsJsonArray();
				cordenadas = cordenadas.get(0).getAsJsonArray();



				//inicializamos la lista
				JsonArray listapuntosZona = cordenadas; /* = new Punto[cordenadas.size()];*/


				JsonObject propiedades = ZONA.getAsJsonObject("properties");

				int pcartodb_id =  propiedades.get("cartodb_id").getAsInt();
				int pscacodigo =propiedades.get("scacodigo").getAsInt();
				int pscatipo =propiedades.get("scatipo").getAsInt();
				String pscanombre = propiedades.get("scanombre").getAsString();
				double pshape_leng = propiedades.get("shape_leng").getAsDouble();
				double pshape_area = propiedades.get("shape_area").getAsDouble();
				int pMOVEMENT_ID = propiedades.get("MOVEMENT_ID").getAsInt();

				ZonasUber a = new ZonasUber(listapuntosZona, pcartodb_id, pscacodigo, pscatipo, pscanombre, pshape_leng, pshape_area, pMOVEMENT_ID);

				ZonasUber2 b = new ZonasUber2(listapuntosZona, pcartodb_id, pscacodigo, pscatipo, pscanombre, pshape_leng, pshape_area, pMOVEMENT_ID);

				contador++;

				System.out.println("La " + i + "-ésima zona es " + a);	


				arbolbalanceado_JSON.put(pMOVEMENT_ID, a);


				//tablaHashR1B.putInSet(pMOVEMENT_ID, a);
				tamano_tablaHashR1B++;

				//priorityQueueZonaNumNodos_R3C.agregar(b);


			}


		} catch (IOException e) {

			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		} 	

		//System.out.println(arbolbalanceadoL.darAltura());
		//System.out.println(arbolbalanceadoL.altura());

		return contador ;


	}



	/**
	 * Este metodo carga en las estructuras de datos datos del archivo malla vial 
	 * @return
	 */
	public int cargarMallaVial()
	{
		
		String archivo_prueba_malla = "./data/Prueba_malla.txt";

		File archivo = new File(archivo_prueba_malla);
		FileReader lector;
		int mallasviales=0;

		try {

			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);

			String linea = bf.readLine();
			bf.readLine();

			int i=0;
			
			while(linea != null )
			{
				String[] datos = linea.split(",");

				int iDNodo = Integer.parseInt(datos[0]);
				double longitud = Double.parseDouble(datos[1]);
				double latitud = Double.parseDouble(datos[2]);

				mallaVial malla = new mallaVial(iDNodo, longitud, latitud);


				arbolbalanceadoMALLA_R2B.put(iDNodo, malla); 


				System.out.println("cargando mallas " + i + "/228045");

				linea = bf.readLine();
				mallasviales++;
				i++;
			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}
		return mallasviales;
	}





	//mayor latitud mas al norte. 
	/**
	 * R1B- Buscar los N zonas que están más al norte. N es un valor de entrada. 
	 * Una zona A esta más al Norte que una zona B si algún punto de la frontera de A está más al norte que todos los puntos de la frontera de B. 
	 * Mostrar las zonas ordenadas desde las que estén más al norte. De cada zona se debe imprimir el nombre y la (latitud, longitud) de su punto más al norte.
	 * 
	 * @param n
	 * @return
	 */
	public ZonasUber[] R1B_BuscarLasNZonasMasAlNorte(int n)
	{

		double latitudMax = 0;


		ZonasUber[] arregloConElementos = new ZonasUber[n];


		//Agrega las Zonas que se encuentran mas al norte a un arreglo a retornar
		for (int i = 0; i < tablaHashR1B.size(); i++) 
		{
			ZonasUber verificar =  (ZonasUber) tablaHashR1B.getSet(i);

			arregloConElementos = arregloConZonas(n, verificar);
		}

		return arregloConElementos;
	}



	/**
	 * 
	 * Este metodo auxiliar buscar y compara cada zona para ver cual es mayor que la otra 
	 * @param n
	 * @param zonaAanalizar
	 * @return
	 */
	public ZonasUber[] arregloConZonas(int n, ZonasUber zonaAanalizar )
	{
		ZonasUber[] arregloConElementos = new ZonasUber[n];
		double[] arregloConPuntoMasAlNorte = new double[n];


		double menorActual=999999999;
		int posicionMenorActual=0;

		int posicion =0;
		boolean yaEstaLaLista= false;




		//Verifica que que haya disponibilidad
		if(arregloConElementos.length < n )
		{
			//Agrega la zona 
			arregloConElementos[posicion] = zonaAanalizar;
			posicion++;
		}
		//caso en que la lista este llena verifica si el nodo pasado por parametro tiene un punto mas al norte que los acutales 
		else
		{		

			if(yaEstaLaLista==false)
			{
				//Se busca el punto mas al norte de todos los puntos de todas las zonas 
				for (int i = 0; i < arregloConElementos.length; i++) 
				{

					JsonArray listapuntos =  arregloConElementos[i].getArreglopuntos();

					double PuntoMasAlNorteZona = buscaMayorLatitudEnArregloDePuntosDeUnaZona(listapuntos);


					arregloConPuntoMasAlNorte[i] = PuntoMasAlNorteZona;

					if( PuntoMasAlNorteZona < menorActual )
					{
						menorActual = PuntoMasAlNorteZona;
						posicionMenorActual = i;
					}

				}	
				yaEstaLaLista =true;
			}
			else
			{

				double puntoMasNorteZonaPorParametro = buscaMayorLatitudEnArregloDePuntosDeUnaZona(zonaAanalizar.getArreglopuntos());  

				//Este if es por si la lita ya esta llena y se encuentra que hay uno mayor a los que ya se tienen en las listas
				if(puntoMasNorteZonaPorParametro > menorActual  )
				{
					arregloConElementos[posicionMenorActual]= zonaAanalizar;

					//Se busca de nuevo quien quedaria siendo el menor actual
					for (int i = 0; i < arregloConElementos.length; i++) 
					{
						JsonArray listapuntos =  arregloConElementos[i].getArreglopuntos();

						double PuntoMasAlNorteZona = buscaMayorLatitudEnArregloDePuntosDeUnaZona(listapuntos);


						arregloConPuntoMasAlNorte[i] = PuntoMasAlNorteZona;

						if( PuntoMasAlNorteZona < menorActual )
						{
							menorActual = PuntoMasAlNorteZona;
							posicionMenorActual = i;
						}
					}
				}
			}
		}
		return arregloConElementos;
	}



	/**
	 * Retorna el valor de la latitud mas al norte de un arreglo de puntos. 
	 * @param lista
	 * @return
	 */
	public double buscaMayorLatitudEnArregloDePuntosDeUnaZona(JsonArray lista)
	{
		double latitudMasAlNorte= 0;
		JsonArray Zona = lista;

		for (int j = 0; j < Zona.size(); j++) 
		{
			String latitudActual = Zona.getAsJsonArray().get(1).getAsString();

			double latitudActuall = Double.parseDouble(latitudActual);

			if (latitudActuall > latitudMasAlNorte){
				latitudMasAlNorte = latitudActuall;
			}

		}	
		return latitudMasAlNorte;
	}

	/**
	 *  PARTE B PUNTO 2
	 * Ordenar las zona  de mayor a menor por el punto mas al norte
	 */
	public ZonasUber[] ordenarDeMayorAMenorMerge (double NodoMasAlNorte , ZonasUber[] arregloConZonasAordenar )
	{

		ZonasUber[] listaConDatos = new ZonasUber[ arregloConZonasAordenar.length] ;



		return mergeSort(0, listaConDatos.length-1, NodoMasAlNorte, listaConDatos);
	}


	/**
	 *  ORDENAMIENTO DE MERGE SORT
	 * @param plow
	 * @param phigh
	 * @param hora
	 * @return
	 */
	public ZonasUber[] mergeSort( double plow, double phigh, double NodoMasAlNorte ,  ZonasUber[] arregloConZonasAordenar)
	{

		ZonasUber[] aux = new ZonasUber[ arregloConZonasAordenar.length ];

		if(plow >= phigh)
		{
			return aux;
		}

		double  middle = (plow+ phigh)/2;

		mergeSort( plow, middle,NodoMasAlNorte, aux);

		mergeSort( middle+1, phigh, NodoMasAlNorte, aux);

		merge (  plow,middle, phigh, NodoMasAlNorte, aux);

		return aux;


	}



	/**
	 *  PARTE B PUNTO 2
	 * @param Plow
	 * @param Pmiddle
	 * @param Phigh
	 * @param dia
	 */
	private void merge ( double Plow, double Pmiddle, double Phigh, double NodoMasAlNorte, ZonasUber[] arregloConZonasAordenar )
	{
		ZonasUber[] listaAOrdenar = new ZonasUber[arregloConZonasAordenar.length];

		ZonasUber[] aux = new ZonasUber[arregloConZonasAordenar.length];

		double i = Plow;
		double j = Pmiddle+1;
		double k = Plow;

		while ((i <= Pmiddle) &&  (j <= Phigh)){   
			if (listaAOrdenar[(int) i].compareTo(listaAOrdenar[(int) j])==(-1|0)){

				listaAOrdenar[(int) k]= aux[(int) i];
				i++;
			}

			else{

				listaAOrdenar[(int) k]= aux[(int) j];
				j++;
			}
		}

		k++;

		while(i<= Pmiddle) {
			listaAOrdenar[(int) k]= aux[(int) i];
			k++;
			i++;
		}

		while(j<= Phigh) {
			listaAOrdenar[(int) k]= aux[(int) j];
			k++;
			j++;
		}
	}










	/**
	 * 2B- Buscar nodos de la malla vial por Localización Geográfica (latitud, longitud).
	 * Dado una latitud y una longitud, se deben mostrar todos los nodos que tengan esas mismas latitud 
	 * y longitud truncando a 2 cifras decimales
	 * 
	 * @param pLongitudDada
	 * @param pLatitudDada
	 * @param pNumElementos
	 * @return
	 */
	public mallaVial[] R2B_BuscarNodosMallaVial (double pLongitudDada, double pLatitudDada, int pNumElementos)
	{

		mallaVial[] lista = new mallaVial[pNumElementos];

		int pos=0;


		double longitudDada = round(pLongitudDada, 2);
		double latitudDada = round(pLatitudDada, 2);


		for (int i = 0; i < arbolbalanceadoMALLA_R2B.size(); i++) 
		{

			//Tiene problemas por que no coge el nodo de malla vial 
			mallaVial verificarCoincidencia = (mallaVial) arbolbalanceadoMALLA_R2B.get(i);

			if(buscarNodosConLongitudYLatitudSimilar(longitudDada, latitudDada, verificarCoincidencia) ==true)
			{
				lista[pos] = verificarCoincidencia;
				pos++;
			}

		}


		return lista;
	}


	/**
	 * Este metodo compara la similitud entre dos nodos y redonde a las cifras decimales
	 * 
	 * @param pLongitudDada es la que va en negativo 
	 * @param pLatitudDada
	 * @return
	 */
	public  boolean  buscarNodosConLongitudYLatitudSimilar(double pLongitudDada, double pLatitudDada, mallaVial pdatoAComparar)
	{

		mallaVial datoAComparar = pdatoAComparar;		


		double longitudNodo = round(datoAComparar.darlongitud(), 2);
		double latitudNodo =round(datoAComparar.darlatitud(), 2);



		if ( longitudNodo == pLongitudDada && latitudNodo == pLatitudDada  )
		{

			return true;
		}


		return false;

	}

	/**
	 * Este metodo fue sacado de stack over flow debido a que intente de la forma con decimal format pero no pude.
	 * @param LongitudOLatitud
	 * @param cifrasDecimales
	 * @return
	 */
	public static double round(double LongitudOLatitud, int cifrasDecimales) 
	{
		if (cifrasDecimales < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, cifrasDecimales);
		LongitudOLatitud = LongitudOLatitud * factor;
		long tmp = Math.round(LongitudOLatitud);
		return (double) tmp / factor;
	}




	/**
	 * 3B- Buscar los tiempos de espera que tienen una desviación estándar en un rango dado y que son del primer trimestre del 2018.
	 * Dado un rango de desviaciones estándares [limite_bajo, limite_alto] retornar los viajes cuya desviación estándar mensual este en ese rango.
	 * 
	 * @param plimiteBajoDesviacion
	 * @param plimiteAltoDesviacion
	 * @param pnumeroDatos
	 * @return
	 */
	public viajeUberMes[] R3B_BuscarTiemposDeEsperaConRango(double plimiteBajoDesviacion, double plimiteAltoDesviacion, int pnumeroDatos)
	{

		viajeUberMes[] resultado = new viajeUberMes[pnumeroDatos];
		int datos = 0;
		boolean termino= false;
		double Acomparar = PriorityQueu_R3B.darElemento(1).darstandardDeviationTravelTime();


		while (datos < pnumeroDatos && !termino) {

			//verifica si la desviacion pasada por parametro es mayor al limete mayor que se pide y en caso de que si lo elimina
			if(Acomparar > plimiteAltoDesviacion  ){
				PriorityQueu_R3B.eliminarMax();
			}

			//verifica si el numero de desviacion estandar esta en el rango dado ya sea que sea igual al menor o igual al mayor o que entre estos dos 
			else if( Acomparar==plimiteAltoDesviacion || Acomparar== plimiteBajoDesviacion || (Acomparar > plimiteBajoDesviacion && Acomparar< plimiteAltoDesviacion)  ){

				resultado[datos] = PriorityQueu_R3B.darElemento(1);

				datos++;
			}
			//Verifica si la desviacion pasada por parametro es menor al limite bajo y en ese caso la elimina
			else if(Acomparar< plimiteBajoDesviacion){
				termino = true;
			}	
		}

		return resultado;
	}


	/**
	 * Obtener las N zonas priorizadas por la mayor cantidad de nodos que definen su frontera. El valor N es un dato de entrada. 
	 * Por cada zona se debe mostrar el nombre de la zona y el número de nodos que definen su frontera.
	 */
	public ZonasUber2[] R3C_ObetenerLasNZonasPriorizadas (int N)
	{

		ZonasUber2[] respuestaConLaszona = new ZonasUber2[N];

		//Como la cola se organiza de forma que la prioridad sea la cantidad de nodos solo se devuleve 
		for (int i = 0; i < N; i++) 
		{
			respuestaConLaszona[i]  = priorityQueueZonaNumNodos_R3C.eliminarMax();
		}	

		return respuestaConLaszona;
	}



	/**
	 * 4C – Gráfica ASCII - Porcentaje de datos faltantes para el primer semestre 2018. 
	 * En los datos por horas se considera que cada zona de origen debe tener los tiempos de viaje hacia todas las otras zonas en todas las horas y en cada trimestre. 
	 * Por ejemplo, si hubiera 2 zonas (1 y 2), se espera que hayan 96 viajes que salen de la zona 1 durante el primer semestre 2018:
	 * @param zonaOrigen
	 */
	public int R4C_RetornaElNumeroDeDatosFaltantes (int zonaOrigen )
	{
		//Total de viajes que pueden llegar a haber
		int TotalSupuestoViajes = 111360;

		viajeUberHora[] viajesConzonaorigenParametro = BuscaLosviajesQueSalendeLaZonaPorParametro(zonaOrigen)  ;

		int NumViajesFaltantes =  TotalSupuestoViajes - viajesConzonaorigenParametro.length;

		return NumViajesFaltantes;
	}



	/**
	 * Retorna el total de viajes que salen de una zona. 
	 * 
	 * @param zonaOrigen
	 * @return
	 */
	public viajeUberHora[] BuscaLosviajesQueSalendeLaZonaPorParametro (int zonaOrigen )
	{
		int NumeroViajesNoExistentes = 0;

		int tam = 0;

		viajeUberHora[] todosViajesHorasTodosSemestres = new viajeUberHora[arbolbalanceadoTablaAscii.size() ];

		viajeUberHora[] viajesConzonaorigenParametro = new viajeUberHora[tam];

		for (int i = 0; i < todosViajesHorasTodosSemestres.length; i++) 
		{

			int trimestre = 1|2|3|4;
			int num;



			for (int j = 0; j < 1160; j++) 
			{
				String llave =( trimestre+"-"+ zonaOrigen+"-"+ j);

				viajeUberHora verificar =  (viajeUberHora) arbolbalanceadoTablaAscii.get(llave);

				if(verificar.darsourceId() == zonaOrigen )
				{
					viajesConzonaorigenParametro[tam] = verificar;
					tam++;

					arbolbalanceadoTablaAscii.delete(llave);

				}		
			}	

		}
		return viajesConzonaorigenParametro;
	}



	/**
	 * revisa cuantos datos faltan de una zona a otra en las horas dadas
	 * @return
	 */
	public int RevisaDatosFaltantesEnUN_TRIMESTRE_DE_ZONA1_A_ZONA2()
	{

		int numerosFaltantesHora=0;
		int numelementosExistentes=0;

		viajeUberHora[] arreglo = new  viajeUberHora[13];

		for (int i = 0; i < 24 ; i++) {
			for (int j = 0; j < arreglo.length; j++) {
				if(arreglo[j] .darHod()==i );
				{
					numelementosExistentes++;
				}	
			}
		}
		numerosFaltantesHora = 24- numelementosExistentes; 

		return numerosFaltantesHora;
	}


	/**
	 * Este metodo grafica los asteriscos donde.
	 * 
	 * Cada * representa un 2% de viajes faltantes.
	 * @param NumeroViajesNoExistentes
	 * @return
	 */
	public String graficaAsterisco(int NumeroViajesNoExistentes)
	{
		int totalDatosZona = 111360;
		String asterisco = "*";
		String cadenaAsteriscos = "";

		double PorcentajeRedondeado = (( (NumeroViajesNoExistentes*100)/totalDatosZona ))  ;

		double numeroAsteriscos = (int) ((PorcentajeRedondeado/2));

		for (int i = 0; i < numeroAsteriscos ; i++) {
			if(i==0){
				cadenaAsteriscos = asterisco;
			}
			else{
				cadenaAsteriscos = cadenaAsteriscos + asterisco; 						
			}	
		}
		return cadenaAsteriscos;
	}

	//Todos los metosoa que estan en la clase Requerimiento reciben su lectura de
	// archivo en una estructura de datos aqui.
	/**
	 * Inicializaciond del metodo que se encuentra en Clase 1A 
	 * el metodo es el 1A (Este metodo pasa por parametro la estructura que
	 * carga los archivos)
	 * @param N
	 * @return  un arreglo de strings
	 */
	public String[] requerimiento1A(int N)
	{

		return reqA.requerimiento1A(N, arbolbalanceado_JSON);
	}
	/**
	 * Inicializacion del metodo que se encuentra en la clase 
	 * requerimientoA el metodo es numeroDeNodos que soluciona 
	 *el requerimientos 2A
	 * @param pArbolBalanceado
	 * @param pLatitud
	 * @param pLongitud
	 * @return un arbolBalanceado
	 */
	public ArbolBalanceadoRojoNegro<Integer, Object[]> requerimientoA2(double pLatitud, double pLongitud){
		return reqA.numeroDeNodos(arbolbalanceadoMALLA_R2B, pLatitud, pLongitud);
	}

	public MaxHeapCP<viajeUberMes> requerimiento3A(
			int pLimite_bajo, int pLimite_alto, int nViajesOrdenados){
		return reqA.requerimientoParte3A(PriorityQueu_R3B, pLimite_bajo, pLimite_alto, nViajesOrdenados);
	}

	public  ListaEnlazada <viajeUberHora> requerimientos1C(int pZonadada,int pHoraDada){
		
		return reqA.requerimientosParte1C(tablaHashHoras, pZonadada, pHoraDada);
	}

	public ListaEnlazada<viajeUberHora> requerimientoParteC2(int pNombreZona, int pHoraInicial, int pHoraFinal) {
		System.out.println("Llama al controller");
		 return reqA.requerimientoParteC2(tablaHashHoras, pNombreZona, pHoraInicial, pHoraFinal);
	}
}















