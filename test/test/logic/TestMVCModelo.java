package test.logic;

import static org.junit.Assert.*;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import model.logic.MVCModelo;
import model.logic.UBERTrip;
import org.junit.Before;
import org.junit.Test;

public class TestMVCModelo {

	private MVCModelo modelo;





	/**
	 * Caso en el cual la lista esta organizada ascendentemente 
	 */
	@Before
	public UBERTrip[] setUpEscenario1() 
	{
		UBERTrip[] iter = new UBERTrip[20];
		
		for (int i = 0; i < 20; i++) 
		{
			UBERTrip viaje = new UBERTrip(i, i, i, i, i, i, i);
			iter[i] = viaje ;
		
		}
		
		return iter;
	}
	/**
	 * Caso en el cual la lista esta desorganizada
	 * 
	 */
	public UBERTrip[] setUp2() 
	{
		UBERTrip[] iter = new UBERTrip[20];

		for (int i = 0; i < 20; i--) 
		{
			UBERTrip viaje = new UBERTrip(i, i, i, i, i, i, i);
			iter[i] = viaje ;
		}
		//generan valores al azar 
		Random rnd = ThreadLocalRandom.current();
		for (int i = iter.length - 1; i > 0; i--)
		{
			int index = rnd.nextInt(i + 1);

			UBERTrip a = iter[index];
			iter[index] = iter[i];
			iter[i] = a;
		}
		return iter;
	}

	/**
	 * Se crea lista de datos desordenados 
	 */
	public UBERTrip[] setupEscenario3()
	{

		UBERTrip[] iter = new UBERTrip[20];

		UBERTrip menor;

		for (int i = 20; i > 20; i--) 
		{
			UBERTrip viaje = new UBERTrip(i, i, i, i, i, i, i);
			iter[i] = viaje ;
		}
		
		return iter;
	}
	
	



	public void setup3()
	{

		UBERTrip[] iter = new UBERTrip[20];

		UBERTrip menor;

		for (int i = 0; i < 20; i++) 
		{
			UBERTrip viaje = new UBERTrip(i, i, i, i, i, i, i);
			iter[i] = viaje ;
		}


		for(int i = 0; i < 20; i++)
		{
			menor = iter[0];

			if (iter[i].compareTo(menor)==-1  )
			{
				menor = iter[i];
			}
			else{
				if (iter[i].compareTo(menor)==1)
				{
					menor = menor;
				}      
			}
		}
	}
	
	/**Las pruebas unificadas de shellSort
	 * 
	 */
	@Test 
	public void testShellSort() {
		setUpEscenario1();
		setUp2();
		setupEscenario3();
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.shellSort(setUpEscenario1().length));
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.shellSort(setUp2().length));
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.shellSort(setupEscenario3().length));
	}
	
	@Test 
	public void testMergeSort() {
		setUpEscenario1();
		setUp2();
		setupEscenario3();
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.mergeSort(setUpEscenario1().length, setUp2().length, setUpEscenario1().length));
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.mergeSort(setUpEscenario1().length, setUp2().length, setUp2().length));
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.mergeSort(setUpEscenario1().length, setUp2().length, setupEscenario3().length));
	}
	@Test 
	public void testQuickSort() {
		setUpEscenario1();
		setUp2();
		setupEscenario3();
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.quickSort(setUpEscenario1().length, setUpEscenario1().length, setUpEscenario1().length));
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.quickSort(setUpEscenario1().length, setUpEscenario1().length, setUp2().length));
			assertEquals("Se pudo ordenar el arreglo de horas",setUpEscenario1().length , modelo.quickSort(setUpEscenario1().length, setUpEscenario1().length, setupEscenario3().length));
	}

	@Test
	public void testMVCModelo() {
		assertTrue(modelo!=null);
		//assertEquals(0, modelo.darTamano());  // Modelo con 0 elementos presentes.
	}

	@Test
	public void testDarTamano() {
		//assertEquals(0, modelo.darTamano());  // Modelo con 0 elementos presentes.
	}

	@Test
	public void testAgregar() {
		

	}

	@Test
	public void testBuscar() {
		setUp2();
		
	}

	@Test
	public void testEliminar() {
		setUp2();
		

	}

}
