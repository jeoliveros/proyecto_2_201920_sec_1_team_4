package test.data_structures;

import java.awt.List;
import java.util.ArrayList;

import model.data_structures.Nodo;



public class PruebaColaPrioridad <T extends Comparable<T>>{
 
	public T elemento;
	public Nodo<T> elementoSiguiente;
	
	public <T> void setUp1() {
		ArrayList<T> listaEscenario1 = new ArrayList<T>();
		
		listaEscenario1.add((T) elemento);
		listaEscenario1.add( (T) elementoSiguiente.darSiguiente());
	}
}
