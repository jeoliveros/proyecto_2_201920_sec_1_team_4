package test.data_structures;

import model.data_structures.Nodo;
import model.data_structures.ArbolBalanceadoRojoNegro;

public class RedBlasckBSTTest {
	//Atributos
ArbolBalanceadoRojoNegro arbolBalanceado;
Nodo raiz;
/**
 * Codigo obtenido de https://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html
 * y modificado para propio entendimiento
 * @return
 */
	    private boolean check() {
	        if (!isBST())            System.out.println("No esta en orden simetrico este arbol");
	        if (!isSizeConsistent()) System.out.println("El conteo de los subArboles no es consistente");
	        if (!esRangoConsistente()) System.out.println("rango no consistente");
	        if (!is23())             System.out.println("No es un arbol 2-3");
	        if (!isBalanced())       System.out.println("No esta balanceado");
	        return isBST() && isSizeConsistent() && esRangoConsistente() && is23() && isBalanced();
	    }

	    // does this binary tree satisfy symmetric order?
	    // Note: this test also ensures that data structure is a binary tree since order is strict
	    private boolean isBST() {
			return isBST(null, null, null);
	    }

	    // is the tree rooted at x a BST with all keys strictly between min and max
	    // (if min or max is null, treat as empty constraint)
	    // Credit: Bob Dondero's elegant solution
	    private boolean isBST(Nodo x, Object min, Object max) {
	        if (x == null) return true;
	        if (min != null && x.darLlaveCorrespondienteNodo(x).compareTo(min) <= 0)
	        	return false;
	        if (max != null && x.darLlaveCorrespondienteNodo(x).compareTo(max) >= 0)
	        	return false;
	        return isBST(x.NodoDerecha(), min, x.NodoIzquierda()) && isBST(x.NodoDerecha(), x.darLlaveCorrespondienteNodo(x), max);
	    } 

	    // are the size fields correct?
	    private boolean isSizeConsistent() { return isSizeConsistent(); }
	    private boolean isSizeConsistent(Nodo x) {
	        if (x == null)
	        	return true;
	        if (x.size() != arbolBalanceado.size(x.NodoIzquierda()) + arbolBalanceado.size(x.NodoDerecha()) + 1)
	        	return false;
	        return isSizeConsistent(x.NodoIzquierda()) && isSizeConsistent(x.NodoDerecha());
	    } 
/**
 * Verificar que los rangos sean consistentes
 * @return si el rango es consistente si o no 
 */
	    private boolean esRangoConsistente() {
	        for (int i = 0; i < raiz.size(); i++)
	            if (i != rank(raiz.select(i))) { 
	            	return false;
	            }
	        for (Key key : keys())
	            if (key.compareTo(select(rank(key))) != 0) {
	            	return false;
	            }
	        return true;
	    }

	    // Does the tree have no red right links, and at most one (left)
	    // red links in a row on any path?
/*	    private boolean is23() { return is23(root); }
	    private boolean is23(Nodo x) {
	        if (x == null) return true;
	        if (isRed(x.NodoDerecha())) return false;
	        if (x != root && isRed(x) && isRed(x.left))
	            return false;
	        return is23(x.left) && is23(x.right);
	    } 

	    // do all paths from root to leaf have same number of black edges?
	    private boolean isBalanced() { 
	        int black = 0;     // number of black links on path from root to min
	        Node x = root;
	        while (x != null) {
	            if (!isRed(x)) black++;
	            x = x.left;
	        }
	        return isBalanced(root, black);
	    }

	    // does every path from the root to a leaf have the given number of black links?
	    private boolean isBalanced(Nodo x, int black) {
	        if (x == null) return black == 0;
	        if (!isRed(x)) black--;
	        return isBalanced(x.left, black) && isBalanced(x.right, black);
	    } 


	    /**
	     * Unit tests the {@code RedBlackBST} data type.
	     *
	     * @param args the command-line arguments
	     */
/*	    public static void main(String[] args) { 
	        arbolBalanceado<String, Integer> st = new arbolBalanceado<String, Integer>();
	        for (int i = 0; !StdIn.isEmpty(); i++) {
	            String key = StdIn.readString();
	            st.put(key, i);
	        }
	        for (String s : st.keys())
	            StdOut.println(s + " " + st.get(s));
	        StdOut.println();
	    }
	}
*/
}
