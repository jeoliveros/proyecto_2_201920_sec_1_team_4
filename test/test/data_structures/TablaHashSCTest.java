package test.data_structures;



import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.*;

/**
 * Pruebas para la tabla de hash que utiliza linear probing para solucionar colisiones
 * @author pa.suarezm
 */
public class TablaHashSCTest <K, V> extends TestCase{



	private  K Llave = null;
	private V valor = null;
	//Atributos

	
	/**
	 * La tabla de prueba
	 * Las llaves para esta tabla son K y almacena valores de tipo Object
	 */
	private TablaHashSC <K,V> tabla;
	
	
	//Escenarios
	
	
	/**
	 * Inicializa la tabla vacia con un tamanio de 5
	 */
	public void setUp(){
		tabla = new TablaHashSC(5);
	}
	

	//Tests

	
	/**
	 * Caso 1: Agrega una dupla que no existe
	 * Caso 2: Agrega una dupla en donde la llave ya existe 
	 */
	public void testPut(){
		setUp();
		
		//Caso 1
		K  llave1 = Llave ;
		V valor1 = valor; 
		tabla.putInSet(llave1, valor);
		assertEquals("El valor no se agrego bien", 
						1, tabla.getSet(llave1));
		
		//Caso 2
		tabla.putInSet(llave1, valor);
		assertEquals("El valor no se agrego bien",
						Integer.valueOf(2), tabla.getSet(llave1));

	}
	
	/**
	 * Caso 1: Return el valor agregado anteriormente con la misma llave
	 * Caso 2: Return null con una llave que no existe
	 */
	public void testGet(){
		setUp();
		
		//Caso 1
		K llave1 = Llave;
		tabla.putInSet(llave1, valor);
		assertEquals("No retornar el valor esperado", Integer.valueOf(1), tabla.getSet(llave1));
		
		//Caso 2
		assertNull("No retornar el valor esperado", tabla.getSet(llave1));
	}
	
	/**
	 * Caso 1: Borra un valor que existe en la tabla
	 * Caso 2: Intenta borrar un valor que no existe en la tabla
	 */
	public void testDelete(){
		setUp();
		
		//Caso 1
		K llave1 = Llave;
		tabla.putInSet(llave1, valor);
		assertEquals("No devuelve el valor eliminado", Integer.valueOf(1), tabla.deleteSet(llave1));
		assertNull("No elimin� el valor", tabla.getSet(llave1));
		
		//Caso 2
		assertNull("No devuelve el valor esperado", tabla.getSet(llave1));
		
	}
	
	/**
	 * Caso 1: Verifica que lo que retorne keys() sea una instancia de Iterator y que no sea null
	 */
	public void testKeys(){
		setUp();
		
		assertTrue("Deberia retornar un Iterator", (tabla.keys() instanceof Iterator<?>) 
				&& (tabla.keys()!=null));
	}
	
	
}
